const express = require("express");
const app = express();
const cors = require("cors");
const bodyParser = require("body-parser");

app.disable('x-powered-by');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors());


// Memory data

const favourtiresLists = [];

const getFavourites = () => {
    return favourtiresLists;
}

const addFavourites = (elem) => {
    if (favourtiresLists.findIndex(x => x.name === elem.name) !== -1) {
        return false;
    } else {
        favourtiresLists.push(elem);
        return true;
    }
}

app.set('getFavourites', getFavourites);
app.set('addFavourites', addFavourites);

// Server start

app.listen(3000, () => {
    console.log(`Servidor iniciado en el puerto 3000.`)
})

app.get('/ping', (req, res) => {
    res.send(`El servidor está activo.`);
});

app.use('/', require('./src/routes'));