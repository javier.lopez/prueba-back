module.exports = (req, res, next) => {
    const getFavourites = req.app.get('getFavourites');
    console.log('Method /favourites GET called.');
    return res.status(200).send({
        status: true,
        response: {
            message: 'OK.',
            favourites: getFavourites()
        }
    });
}