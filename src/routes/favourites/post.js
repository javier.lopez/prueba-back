const checkBodyParams = (body) => {
    if (!body) {
        return false;
    } else if (!body.name || !body.userList) {
        return false
    } else {
        return true;
    }
}
module.exports = (req, res, next) => {
    const getFavourites = req.app.get('getFavourites');
    const addFavourites = req.app.get('addFavourites');
    console.log('Method /favourites POST called.');
    if (!checkBodyParams(req.body)) {
        return res.status(400).send({
            status: false,
            response: {
                message: 'Error in body.'
            }
        });
    } else {
        if (addFavourites(req.body)) {
            return res.status(200).send({
                status: true,
                response: {
                    message: 'Created successfully.',
                    favourites: getFavourites()
                }
            });
        } else {
            return res.status(409).send({
                status: false,
                response: {
                    message: 'Name already exists.'
                }
            });
        }
    }
}