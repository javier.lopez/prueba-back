const router = require('express').Router();

router.use('/favourites', require('./favourites'));

module.exports = router;